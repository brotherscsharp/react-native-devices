import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Accelerometer, Gyroscope, ThreeAxisMeasurement, Barometer, BarometerMeasurement, Magnetometer } from 'expo-sensors';

export default function SensorsComponent() {
  const [expoAccelerometer, setAccelerometerData] = useState({} as ThreeAxisMeasurement);
  const [epxoGyroscope, setGyroscopeData] = useState({} as ThreeAxisMeasurement);
  const [epxoBarometer, setBarometerData] = useState({} as BarometerMeasurement);
  const [epxoMagnetometer, setMagnetometerData] = useState({} as ThreeAxisMeasurement);

  var _accelerometerSubscription: any;
  var _gyroscopeSubscription: any;
  var _barometerSbscription: any;  
  var _magnetometerSubscription: any;  
  
  var isSubscribed = false;

  useEffect(() => {
    _toggle();
  }, []);

  useEffect(() => {
    return () => {
      _unsubscribe();
    };
  }, []);

  const _toggle = () => {
    if (isSubscribed) {
      _unsubscribe();
    } else {
      _subscribe();
    }
  };

  const _slow = () => {
    Accelerometer.setUpdateInterval(1000);
    Gyroscope.setUpdateInterval(1000);
    Magnetometer.setUpdateInterval(1000);
  };

  const _fast = () => {
    Accelerometer.setUpdateInterval(16);
    Gyroscope.setUpdateInterval(16);
    Magnetometer.setUpdateInterval(16);
  };

  const _subscribe = () => {
    _accelerometerSubscription = Accelerometer.addListener(accelerometerData => {
      setAccelerometerData(accelerometerData);
    });
    
    _gyroscopeSubscription = Gyroscope.addListener(gyroscopeData => {
      setGyroscopeData(gyroscopeData);      
      //console.log('>>> barometer', epxoBarometer);
    });


    _barometerSbscription = Barometer.addListener(barometerData => {
      setBarometerData(barometerData);
    });

    _magnetometerSubscription = Magnetometer.addListener(result => {
      setMagnetometerData(result);
    });
    isSubscribed = true;
  };

  const _unsubscribe = () => {
    isSubscribed = false;
    _accelerometerSubscription && _accelerometerSubscription.remove();
    _accelerometerSubscription = null;

    _gyroscopeSubscription && _gyroscopeSubscription.remove();
    _gyroscopeSubscription = null;

    _barometerSbscription && _barometerSbscription.remove();
    _barometerSbscription = null;

    _magnetometerSubscription && _magnetometerSubscription.remove();
    _magnetometerSubscription = null;
    
  };

  return (
    <View style={styles.sensor}>
      <Text style={styles.headerText}>Accelerometer: (in Gs where 1 G = 9.81 m s^-2)</Text>
      { expoAccelerometer ? (
        <Text style={styles.text}>
          x: {round(expoAccelerometer.x)} y: {round(expoAccelerometer.y)} z: {round(expoAccelerometer.z)}
        </Text>) :
        (<Text style={styles.text}>
          x:  y:  z: }
        </Text>)
      }
      
      <Text style={styles.headerText}>Gyroscope:</Text>
      {epxoGyroscope ? (
        <Text style={styles.text}>
          x: {round(epxoGyroscope.x)} y: {round(epxoGyroscope.y)} z: {round(epxoGyroscope.z)}
        </Text>):
        (<Text style={styles.text}>
          x:  y:  z: 
        </Text>)
      }

      <Text style={styles.headerText}>Barometer:</Text>
      {epxoBarometer && epxoBarometer.pressure ? (
        <Text style={styles.text}>
          {epxoBarometer.pressure * 100} Pa
        </Text>):
        (<Text style={styles.text}>0 Pa</Text>)
      }
      
      <Text style={styles.headerText}>Magnetometer:</Text>
      {epxoMagnetometer ? (
        <Text style={styles.text}>
          x: {round(epxoMagnetometer.x)} y: {round(epxoMagnetometer.y)} z: {round(epxoMagnetometer.z)}
        </Text>):
        (<Text style={styles.text}>
          x:  y:  z: 
        </Text>)
      } 
      
      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={_toggle} style={styles.button}>
          <Text>Toggle</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={_slow} style={[styles.button, styles.middleButton]}>
          <Text>Slow</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={_fast} style={styles.button}>
          <Text>Fast</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

function round(n: any) {
  if (!n) {
    return 0;
  }
  return Math.floor(n * 100) / 100;
}

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: 15,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    padding: 10,
  },
  middleButton: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#ccc',
  },
  sensor: {
    marginTop: 45,
    paddingHorizontal: 10,
  },
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  }
});

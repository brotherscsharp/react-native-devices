import React, {Component, useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Camera} from 'expo-camera';
import * as Permissions from 'expo-permissions';
import * as FileSystem from 'expo-file-system';

export interface CameraState {
  type: any;
  hasPermission: boolean;
  isVideoRecording: boolean;
}
export default class CameraComponent extends Component<{}, CameraState> {
  private camera: any;

  constructor(props: Readonly<{}>) {
    super(props);

    this.state = {
      type: Camera.Constants.Type.back,
      hasPermission: false,
      isVideoRecording: false,
    };
  }

  componentDidMount() {
    // (async () => {
    //   const { status } = await Camera.requestPermissionsAsync();
    //   this.setState({hasPermission: status === 'granted'})
    // })();
  }

  private setCamera = (cam: Camera | null) => {
    this.camera = cam;
  };

  private takePhoto = async () => {
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);
      //console.log(data.uri);
      await this.saveFile(data.uri);
    }
  };

  private saveFile = async (url: string, isPhoto: boolean = true) => {
    console.log(url);
    if (url) {
      const dirName = `${FileSystem.documentDirectory}${
        isPhoto ? 'images' : 'video'
      }`;
      const file = `${dirName}/file_${Date.now()}.png`;
      try {
        // create directoyr if need
        if (!(await FileSystem.getInfoAsync(dirName)).exists) {
          await FileSystem.makeDirectoryAsync(dirName);
        }

        // save file
        await FileSystem.moveAsync({
          from: url,
          to: file,
        });
      } catch (error) {
        console.log('>>>>> errrooorr ', error);
      }
    }
  };

  private videoFileUri: string = '';
  private recordVideo = async () => {
    if (this.camera) {
      console.log('>>> video rec', this.state.isVideoRecording);
      if (this.state.isVideoRecording) {
        this.setState({isVideoRecording: false});
        console.log('>>> stop video rec');
        const data = await this.camera.stopRecording();
        this.saveFile(this.videoFileUri);
        this.videoFileUri = '';
      } else {
        this.setState({isVideoRecording: true});
        this.checkPermissions();
        console.log('>>> start video rec');
        const ratios = await this.camera.getSupportedRatiosAsync();
        const options = {quality: Camera.Constants.VideoQuality[ratios[0]]};
        const data = await this.camera.recordAsync(options);
        this.videoFileUri = data.uri;
      }
    }
  };

  private checkPermissions = async () => {
    let status = await Permissions.askAsync(
      Permissions.CAMERA,
      Permissions.CAMERA_ROLL,
      Permissions.AUDIO_RECORDING,
    );
    this.setState({hasPermission: status.status === 'granted'});
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <Camera
          style={{flex: 1}}
          type={this.state.type}
          ref={(ref) => {
            this.setCamera(ref);
          }}>
          <View style={styles.buttonsMainContainer}>
            <View style={styles.buttonsView}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => {
                  this.setState({
                    type:
                      this.state.type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back,
                  });
                }}>
                <Text style={styles.btnsText}> Flip </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnPhoto}
                onPress={() => this.takePhoto()}>
                <Text style={styles.btnsText}> Take photo </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnVideo}
                onPress={() => this.recordVideo()}>
                <Text style={styles.btnsText}> Video </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Camera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsMainContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  buttonsView: {
    flex: 1,
    alignSelf: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnFlip: {
    alignSelf: 'flex-start',
    alignItems: 'center',
  },
  btnPhoto: {
    alignItems: 'center',
    alignSelf: 'center',
  },
  btnVideo: {
    alignSelf: 'flex-end',
  },
  btnsText: {
    fontSize: 18,
    marginBottom: 10,
    color: 'white',
  },
});

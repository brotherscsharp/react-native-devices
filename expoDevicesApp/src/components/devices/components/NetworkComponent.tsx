import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Network from "expo-network";
import { NetworkState } from "expo-network";

export default function NetworkComponent() {
  const [networkState, setNetwokrState] = useState({} as NetworkState);
  const [networkIP, setNetwokrIP] = useState("");
  const [networkMAC, setNetwokrMAC] = useState("");
  useEffect(() => {
    loadFormData();
  }, []);

  const loadFormData = async () => {
    const nState = await Network.getNetworkStateAsync();
    setNetwokrState(nState);

    const nIP = await Network.getIpAddressAsync();
    setNetwokrIP(nIP);

    const nMac = await Network.getMacAddressAsync();
    setNetwokrMAC(nMac);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>Network state:</Text>
      <Text style={styles.text}>
        Type: {networkState.type} Connected: {`${networkState.isConnected}`}{" "}
        InternetReachable: {`${networkState.isInternetReachable}`}
      </Text>
      <Text style={styles.headerText}>IP address:</Text>
      <Text style={styles.text}>{networkIP}</Text>
      <Text style={styles.headerText}>MAC address:</Text>
      <Text style={styles.text}>{networkMAC}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  text: {
    textAlign: "center",
  },
  headerText: {
    marginTop: 20,
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
  },
});

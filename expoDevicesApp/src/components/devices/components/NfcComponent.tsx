import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function NfcComponent() {
  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>Comming soon...</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

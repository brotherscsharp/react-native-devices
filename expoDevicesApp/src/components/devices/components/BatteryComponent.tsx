import React from 'react';
import * as Battery from 'expo-battery';
import {StyleSheet, Text, View} from 'react-native';

export default class BatteryComponent extends React.Component {
  state = {
    batteryLevel: null,
    batteryState: null,
    // batteryPowerState: null,
    batteryIsLowPowerEnabled: null,
  };

  _subscriptionBatteryMode: any;
  _subscriptionBatteryState: any;
  _subscriptionBatteryLevel: any;

  async componentDidMount() {
    //let batteryLevel = await Battery.getBatteryLevelAsync();
    //let batteryState = await Battery.getBatteryStateAsync();
    let batteryPowerState = await Battery.getPowerStateAsync(); // returns {batteryLevel: 1
    // batteryState: 2
    // lowPowerMode: false}
    //let batteryIsLowPowerEnabled = await Battery.isLowPowerModeEnabledAsync();
    this.setState({
      batteryLevel: batteryPowerState.batteryLevel,
      batteryState: batteryPowerState.batteryState,
      batteryIsLowPowerEnabled: batteryPowerState.lowPowerMode,
    });
    this._subscribe();
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  _subscribe = () => {
    this._subscriptionBatteryLevel = Battery.addBatteryLevelListener(
      ({batteryLevel}) => {
        this.setState({batteryLevel});
      },
    );

    this._subscriptionBatteryState = Battery.addBatteryStateListener(
      ({batteryState}) => {
        this.setState({batteryState});
      },
    );

    this._subscriptionBatteryMode = Battery.addLowPowerModeListener(
      ({lowPowerMode}) => {
        this.setState({batteryIsLowPowerEnabled: lowPowerMode});
      },
    );
  };

  _unsubscribe = () => {
    this._subscriptionBatteryLevel && this._subscriptionBatteryLevel.remove();
    this._subscriptionBatteryLevel = null;

    this._subscriptionBatteryState && this._subscriptionBatteryState.remove();
    this._subscriptionBatteryState = null;

    this._subscriptionBatteryMode && this._subscriptionBatteryMode.remove();
    this._subscriptionBatteryMode = null;
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.headerText}>Battery Level:</Text>
        <Text style={styles.text}>{this.state.batteryLevel}</Text>

        <Text style={styles.headerText}>Battery state:</Text>
        <Text style={styles.text}>{this.state.batteryState}</Text>

        <Text style={styles.headerText}>Battery is Low power enabled:</Text>
        <Text style={styles.text}>{this.state.batteryIsLowPowerEnabled}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Platform, Alert, Button} from 'react-native';
import * as LocalAuthentication from 'expo-local-authentication';
import {AuthenticationType} from 'expo-local-authentication';

export default function FingerprintComponent() {
  const [hasFingerprint, setHasFingerprint] = useState(false);
  const [supportedTypes, setSupportedTypes] = useState(
    [] as AuthenticationType[],
  ); //AuthenticationType[]
  const [isEnrolled, setIsEnrolled] = useState(false);
  const [fingerPrintResult, setFingerPrintResult] = useState('');

  useEffect(() => {
    loadFormData();
  }, []);

  const loadFormData = async () => {
    const compatible = await LocalAuthentication.hasHardwareAsync(); // Determine whether a face or fingerprint scanner is available on the device.
    setHasFingerprint(compatible);

    const fingerprints = await LocalAuthentication.isEnrolledAsync(); // Determine whether the device has saved fingerprints or facial data to use for authentication
    setIsEnrolled(fingerprints);

    const supportedTypeArray = await LocalAuthentication.supportedAuthenticationTypesAsync(); //Determine what kinds of authentications are available on the device.
    setSupportedTypes(supportedTypeArray);
  };
  const scanFingerprint = async () => {
    //Attempts to authenticate via Fingerprint/TouchID (or FaceID if available on the device).
    let result = await LocalAuthentication.authenticateAsync({
      promptMessage: 'Scan your finger.',
    });
    setFingerPrintResult(JSON.stringify(result));
  };

  const showAndroidAlert = () => {
    Alert.alert(
      'Fingerprint Scan',
      'Place your finger over the touch sensor and press scan.',
      [
        {
          text: 'Scan',
          onPress: () => {
            scanFingerprint();
          },
        },
        {
          text: 'Cancel',
          onPress: async () => await LocalAuthentication.cancelAuthenticate(), // (Android Only) Cancels authentication flow.,
          style: 'cancel',
        },
      ],
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>Compatible Device?</Text>
      <Text style={styles.text}>{hasFingerprint ? 'True' : 'False'}</Text>

      <Text style={styles.headerText}>Fingerprings Saved?</Text>
      <Text style={styles.text}>{isEnrolled ? 'True' : 'False'}</Text>

      <Text style={styles.headerText}>Supported types</Text>
      {supportedTypes.map(type => (
        <Text>{type === 1 ? 'FINGERPRINT' : 'FACIAL RECOGNITION'}</Text>
      ))}

      <Button
        title="Scan"
        onPress={
          Platform.OS === 'android' ? showAndroidAlert : scanFingerprint
        }></Button>
      <Text>{fingerPrintResult}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 18,
    textAlign: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 150,
    height: 60,
    backgroundColor: '#056ecf',
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 30,
    color: '#fff',
  },
});

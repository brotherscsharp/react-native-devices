import React, {useState} from 'react';
import {View, Button, Text, StyleSheet} from 'react-native';
import {Audio} from 'expo-av';
import * as Permissions from 'expo-permissions';

export const AudioComponent = () => {
  const [isGranted, setIsGranted] = useState(false);
  const [recordingDuration, setRecordingDuration] = useState(0);

  const [fileUrl, setFileUrl] = useState('');
  const [gRecording, setgRecording] = useState(new Audio.Recording());

  const soundObject = new Audio.Sound();

  const playAudio = async () => {
    try {
      await soundObject.loadAsync(
        require('../../../../assets/sounds/hello.mp3'),
      );
      await soundObject.playAsync();
    } catch (error) {
      // An error occurred!
    }
  };

  const pauseAudio = async () => {
    try {
      await soundObject.pauseAsync();
    } catch (error) {
      // An error occurred!
    }
  };

  const stopAudio = async () => {
    try {
      await soundObject.stopAsync();
    } catch (error) {
      // An error occurred!
    }
  };

  const updateState = async (status: any) => {
    try {
      setRecordingDuration(status.durationMillis);
    } catch (error) {
      console.log(error); // eslint-disable-line
    }
  };

  async function onRecordPressed() {
    checkPermissions();
    try {
      if (gRecording) {
        gRecording.setOnRecordingStatusUpdate(null);
        //@ts-ignore
        setgRecording(null);
      }

      //await props.setAudioMode({ allowsRecordingIOS: true });

      const recording = new Audio.Recording();
      recording.setOnRecordingStatusUpdate(updateState);
      recording.setProgressUpdateInterval(200);

      setFileUrl('');
      await recording.prepareToRecordAsync(
        Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY,
      );
      await recording.startAsync();

      setgRecording(recording);
    } catch (error) {
      console.log(error); // eslint-disable-line
    }
  }

  async function stopRocording() {
    try {
      await gRecording.stopAndUnloadAsync();
      //await props.setAudioMode({ allowsRecordingIOS: false });
    } catch (error) {
      console.log(error); // eslint-disable-line
    }

    if (gRecording) {
      const fileUrl = gRecording.getURI();
      gRecording.setOnRecordingStatusUpdate(null);
      //@ts-ignore
      setgRecording(null);
      setFileUrl(fileUrl ?? '');
    }
  }

  async function onPlayPausePressed() {
    const {sound} = await Audio.Sound.createAsync({uri: fileUrl});
    sound.playAsync();
  }

  const checkPermissions = async () => {
    let status = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    setIsGranted(status.status === 'granted');
  };

  return (
    <View>
      <View>
        <Text style={styles.headerText}>Audio player</Text>
        <Button title="Play" onPress={() => playAudio()} />
        <Button title="Pause" onPress={() => pauseAudio()} />
        <Button title="Stop" onPress={() => stopAudio()} />

        <Text style={styles.headerText}>Microphone</Text>
        <Text style={styles.text}>recording: {recordingDuration}</Text>
        <Button title="Record" onPress={() => onRecordPressed()} />
        <Button title="Play" onPress={() => onPlayPausePressed()} />
        <Button title="Stop" onPress={() => stopRocording()} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

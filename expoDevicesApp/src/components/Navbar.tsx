import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {INavbar} from '../protocols/INavbar';
import {useNavigation} from '@react-navigation/native';

export const Navbar = (props: INavbar) => {
  const navigation = useNavigation();

  return (
    <View style={styles.navbar}>
      <Text
        style={styles.menuButton}
        // @ts-ignore
        onPress={() => navigation.toggleDrawer()}>
        Menu
      </Text>
      <Text style={styles.text}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  navbar: {
    height: 70,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#3949ab',
    paddingBottom: 10,
  },

  text: {
    color: '#fff',
    fontSize: 20,
  },
  menuButton: {
    position: 'absolute',
    alignSelf: 'flex-start',
    color: '#fff',
    left: 10,
    top: 35,
  },
});

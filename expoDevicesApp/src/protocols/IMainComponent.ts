import {IChildBase} from './IChildBase';

export interface IMainComponent {
  title: string;
  component: any;
}

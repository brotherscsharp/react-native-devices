import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import BatteryComponent from '../components/devices/components/BatteryComponent';

export const BatteryScreen = () => (
  <MainComponent title="Battery" component={<BatteryComponent />} />
);

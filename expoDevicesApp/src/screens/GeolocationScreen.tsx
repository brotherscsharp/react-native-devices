import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import GeolocationComponent from '../components/devices/components/GeolocationComponent';

export const GeolocationScreen = () => (
  <MainComponent title="Geolocation" component={<GeolocationComponent />} />
);

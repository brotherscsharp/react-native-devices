import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import FingerprintComponent from '../components/devices/components/FingerprintComponent';

export const FingerprintScreen = () => (
  <MainComponent title="Fingerprint" component={<FingerprintComponent />} />
);

# react-native-devices

0. [Проблемы react-native](#-Проблемы-с-которыми-мне-пришлось-столкнуться-при-работе-с-react-native.)
1. [Камера](#-1.-Камера.)
2. [Геолокация](#-2.-Геолокация.)
3. [Сенсоры](#-3.-Сенсоры.)
4. [Аудио](#-4.-Аудио.) (микрофон)
5. [Батарея](#-5.-Батарея.)
6. [Сеть](#-6.-Сеть.)
7. [Отпечаток пальца](#-7.-Отпечаток-пальца.)
8. [NFC](#-8.-NFC.)
9. [Bluetooth](#-9.-Bluetooth.)
10. [Дополнительно](#-10.-Дополнительно.) (Multidex)

Небольшие замечания по работе с устройствами в React Native. Буквально с первых минут разбора заметил, что работа с [Expo](https://expo.io/) заметно дружелюбнее, чем react-native. Если в Expoвсе работает сразу (или почти сразу), то с react-native вам придется немного поплясать с бубном.

## Проблемы, с которыми мне пришлось столкнуться при работе с react-native.

- iOS. Запуск и смена эмуляторов (решения так и не нашел). Сталкивался с решением кастомизации локального react-native cli, но все же мне не удалось добиться положительного результат.
- iOS. Запуск на реальном устройстве. Все заработало практически сразу, после указания команты разработчика и provision info в настройках приложения. (запуск только через Xcode)
- Android. Постоянные ошибки о неверно настроенном Андроид окружении. В некоторых случаях решается добавлением ANDROID_HOME и JAVA_HOME (естественно после установки Android SKD и рекомендуемой версии JDK)
- Android. Может возникнуть проблема с версией NDK. Для этого вам достаточно открыть настройки Android SDK, выбрать нужную версию и скачать ее.
- Android & iOS. Установка зависимостей после добавления пакета в проект. Если в iOS все может решиться вызовом команды _pod install packagename_ в корне папки ios, то в Android может не так все очевидно. Объясню на примере подключения react-native-camera, вам предстоит выполнить такие действия:

  - settings.gradle добавить

  ```gradle
  include ':react-native-camera'
  project(':react-native-camera').projectDir = new File(rootProject.projectDir, '../../../android')
  ```

  - app build.gradle -> defaultConfig добавить

  ```gradle
  missingDimensionStrategy 'react-native-camera', 'general'
  ```

  - app build.gradle -> defaultConfig добавить

  ```gradle
  implementation project(':react-native-camera')
  ```

  - не забывает об обновлении AndroidManifest.xml

Можно настроить autolinking, но как я понял, не все пакеты поддерживают это.
Надеюсь, ничего не забыл. Перейдем к сравнениям.

## 1. Камера.

[expo-camera](https://docs.expo.io/versions/latest/sdk/camera/) VS [react-native-camera](https://github.com/react-native-community/react-native-camera)
При работе с expo-camera вам предстоит поставить как минимум два пакет - expo-camera и expo-permissions

```
expo install expo-camera
expo install expo-permissions
```

т.к. они переехали из основного пакета expo в соответствующие отдельные пакеты и старый вызов import { Camera, Permissions } from 'expo-camera'; при компиляции выдаст ошибку, хотя в момент разработки он подсветит их как существующие компоненты.

Для подключения react-native-camera придется добавить пакет

```
yarn add react-native-camera@git+https://git@github.com/react-native-community/react-native-camera.git
```

В остальном, в работе они показались мне похожими, набор параметров практически не отличаются.

## 2. Геолокация.

[expo-location](https://docs.expo.io/versions/latest/sdk/location/) VS [react-native-geolocation](https://github.com/react-native-community/react-native-geolocation)

Подключение expo-location выполняется командой

```
expo install expo-location
```

далее в использовании проблем не возникает, стоит лишь руководствоваться актуальной инструкцией: 1. запрос прав на использование геолокации 2. получение координат

Подключение react-native-geolocation выполняется командой

```
yarn add @react-native-community/geolocation
```

так же можно испытать судьбу и выполнить автолинк данной командой:

```
react-native link @react-native-community/geolocation
```

iOS нормально линканулся, но в Android по прежнему пришлось приложить руки.

AndroidManifest.xml

```
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
```

app\build.gradle -> dependencies

```
implementation project(':react-native-community-geolocation')
```

MainApplication.java

```
import com.facebook.react.shell.MainReactPackage;
import com.reactnativecommunity.geolocation.GeolocationPackage;
```

и дополнить\изменить метод getPackages()

```java
protected List<ReactPackage> getPackages() {
    @SuppressWarnings("UnnecessaryLocalVariable")

    List<ReactPackage> packages = Arrays.asList(
    new MainReactPackage(),
    new GeolocationPackage()
    );
    return packages;
}
```

settings.gradle

```
include ':react-native-community-geolocation'
project(':react-native-community-geolocation').projectDir = new File(rootProject.projectDir, '../node_modules/@react-native-community/geolocation/android')
```

_автолинк у меня не обновил app\build.gradle, MainApplication.java и AndroidManifest.xml_

После этого можно приступать к разработке

## 3. Сенсоры.

[expo-sensors](https://docs.expo.io/versions/latest/sdk/sensors/) VS [react-native-sensors](https://react-native-sensors.github.io/docs/Installation.html)

Expo имеет пакет expo-sensors, который позволяет работать с различными сенсовами телефона, такие как:

- Акселерометор
- Гироскоп
- Барометр
- Магнетометр
- Педоментр

Сложностей в работе не замечено, достаточно лишь установить пакет, импортировать необходимый компонет, подписаться на обновления и наслаждаться данными, кторые прилетают от сенсоров

```
expo install expo-sensors
```

```
import {
  Accelerometer,
  Barometer,
  Gyroscope,
  Magnetometer,
  MagnetometerUncalibrated,
  Pedometer,
} from 'expo-sensors';
```

```
_accelerometerSubscription = Accelerometer.addListener(accelerometerData => {
    setAccelerometerData(accelerometerData);
});
```

React Native Sensors имеет практически такой же набор сенсоров, за исключением педометра.
Подключается к пректу без особых проблем, автолинк работает хорошо.

```
npm install react-native-sensors --save
```

```
react-native link react-native-sensors
```

Все как и в Expo, подписываетесь на обновление и используете полученные данные.

## 4. Аудио. (микрофон)

[expo-av](https://docs.expo.io/versions/latest/sdk/audio/) VS [react-native-audio](https://github.com/jsierles/react-native-audio) + [react-native-sound](https://github.com/zmxv/react-native-sound)
expo-av - пакет для работы с аудио, который умеет как воспроизводить аудио так и писать его с микрофона, чего не скажешь про react native, тут придется задействовать два пакета, но в принципе оба варианта сопоставимы по использованию. В пакете expo-av немного больше настроет по качеству аудио.

Пакеты react-native-audio и react-native-sound хорошо линкуются в проекты, по этому с установкой проблем возникнуть не должно.

## 5. Батарея.

[expo-battery](https://docs.expo.io/versions/latest/sdk/battery/) VS [react-native-device-info](https://github.com/react-native-community/react-native-device-info/blob/master/README.md)

expo-battery - максимально прост в работе, нет никаких сложностей с получением данных. Есть возможность запроса информации по отдельности (состояние, уровень, режим), либо одним объектом.
Для установки, просто устанавливаем пакет:

```
expo install expo-battery
```

Пакет react-native-device-info содержит в себе листенеры для получения информации о батарее и хуки, но они довольно "сырые". Если при помощи хуков удается получить информацию, то с листенерами все гораздо хуже, получить данные от них не далось.

react-native содержит в себе DeviceEventEmitter, который так же позволяет получить информацию о батарее, стоит лишь подписаться на одновления таким образом:

```
DeviceEventEmitter.addListener('BatteryStatus', onBatteryStatus);
```

в ответ мы будем получать уровень заряда батареи и состояние кабеля зарядки (подключено\отключено). Данный вариант работает стабильнее и данные актуальные, в отличии от react-native-device-info

## 6. Сеть.

[expo-network](https://docs.expo.io/versions/latest/sdk/network/) VS [react-native-netinfo](https://github.com/react-native-community/react-native-netinfo)

expo-network установка:

```
expo install expo-network
```

react-native-netinfo установка:

```
yarn add @react-native-community/netinfo
react-native link @react-native-community/netinfo
```

Очень прост в использовании и несет исключительно информативный характер. Нет возможности включения\выключения Wi-Fi.

## 7. Отпечаток пальца.

[expo-local-authentication](https://docs.expo.io/versions/latest/sdk/local-authentication/)

```
expo install expo-local-authentication
```

[react-native-biometrics](https://github.com/SelfLender/react-native-biometrics)

## 8. NFC.

Проверить не удалось, т.к. нет девайса. буду искать вариант для тестирования

[react-native-nfc-manager](https://github.com/whitedogg13/react-native-nfc-manager) - явлаяется топовым, по результатам в гугле

[react-native-nfc](https://github.com/Novadart/react-native-nfc)

## 9. Bluetooth.

[react-native-ble-manager](https://github.com/innoveit/react-native-ble-manager)
https://openbase.io/js/react-native-ble-manager

```
npm i --save react-native-ble-manager
```

Права для Android:

```
<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```

Права для iOS:

```
NSBluetoothAlwaysUsageDescription
```

## 10. Дополнительно.

### Multidex

Рано или поздно вы столкнетесь с тем, что нужно будет включить Multidex. Это нужно, если ваше приложение содержит более чем 64000 методов. Для включения нужно будет проделать такие действия:

1. /app/build.gradle

```
android {
    defaultConfig {
        ...
        targetSdkVersion 28
        multiDexEnabled true
    }
    ...
}
dependencies {
  def multidex_version = "2.0.1"
  implementation 'androidx.multidex:multidex:$multidex_version'
}
```

2. MainApplication.java

```
import androidx.multidex.MultiDexApplication;
public class MainApplication extends MultiDexApplication implements ReactApplication {
  ...
}
```

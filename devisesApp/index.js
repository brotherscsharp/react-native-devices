/**
 * @format
 */

// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);

import React, {Component} from 'react';
import {AppRegistry} from 'react-native';

import App from './App';
import {name as appName} from './app.json';

export default class devicesApp extends Component {
  render() {
    return <App />;
  }
}

AppRegistry.registerComponent(appName, () => devicesApp);

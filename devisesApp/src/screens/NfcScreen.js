import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import NfcComponent from '../components/devices/components/NfcComponent';

export const NfcScreen = () => (
  <MainComponent title="NFC" component={<NfcComponent />} />
);

import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import NetworkComponent from '../components/devices/components/NetworkComponent';

export const NetworkScreen = () => (
  <MainComponent title="Network" component={<NetworkComponent />} />
);

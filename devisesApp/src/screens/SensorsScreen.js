import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import SensorsComponent from '../components/devices/components/SensorsComponent';

export const SensorsScreen = () => (
  <MainComponent title="Sensors" component={<SensorsComponent />} />
);

import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import BluetoothComponent from '../components/devices/components/BluetoothComponent';

export const BluetoothScreen = () => (
  <MainComponent title="Bluetooth" component={<BluetoothComponent />} />
);

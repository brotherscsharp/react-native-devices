import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import CameraComponent from '../components/devices/components/CameraComponent';

export const CameraScreen = () => (
  <MainComponent title="Camera" component={<CameraComponent />} />
);

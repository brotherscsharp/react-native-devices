import * as React from 'react';
import {MainComponent} from '../components/MainComponent';
import {AudioComponent} from '../components/devices/components/AudioComponent';

export const AudioScreen = () => (
  <MainComponent title="Audio" component={<AudioComponent />} />
);

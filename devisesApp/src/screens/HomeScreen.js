import * as React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {MainComponent} from '../components/MainComponent';

export const HomeScreen = () => {
  return (
    <MainComponent
      title="Home"
      component={
        <View style={styles.container}>
          <Image source={require('../../assets/sharpdev.png')} />
          <Text style={styles.text}>ReactNative devices cource</Text>
        </View>
      }
    />
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 50,
    alignItems: 'center',
    paddingBottom: 10,
  },
  text: {
    marginTop: 50,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

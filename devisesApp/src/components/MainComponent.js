import * as React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {Navbar} from './Navbar';

export const MainComponent = (props) => {
  return (
    <View>
      <Navbar title={props.title} />
      <View style={styles.container}>{props.component}</View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height - 70,
  },
});

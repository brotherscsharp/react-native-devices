import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Platform, Alert, Button} from 'react-native';
import ReactNativeBiometrics from 'react-native-biometrics';

export default function FingerprintComponent() {
  const [activeSensor, setActiveSensor] = useState({});
  const [fingerPrintResult, setFingerPrintResult] = useState('');
  const [keyCreatedMsg, setkeyCreatedMsg] = useState(false);
  const [keyExistsMsg, setkeyExistsMsg] = useState(false);
  const [keyDeletedMsg, setkeyDeletedMsg] = useState(false);
  const [signatureCreatedMsg, setSignatureCreatedMsg] = useState(false);
  const [fingerPrintSignatue, setfingerPrintSignatue] = useState('');

  useEffect(() => {
    loadFormData();
  }, []);

  const loadFormData = async () => {
    const availableSensor = await ReactNativeBiometrics.isSensorAvailable();
    setActiveSensor(availableSensor);
  };
  const scanFingerprint = async () => {
    ReactNativeBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint'})
      .then((resultObject) => {
        const {success} = resultObject;

        setFingerPrintResult(JSON.stringify(resultObject));
        if (success) {
          console.log('successful biometrics provided');
        } else {
          console.log('user cancelled biometric prompt');
        }
      })
      .catch(() => {
        console.log('biometrics failed');
      });
  };

  const showAndroidAlert = () => {
    Alert.alert(
      'Fingerprint Scan',
      'Place your finger over the touch sensor and press scan.',
      [
        {
          text: 'Scan',
          onPress: () => {
            scanFingerprint();
          },
        },
        {
          text: 'Cancel',
          onPress: async () => console.log('cancel pressed'),
          style: 'cancel',
        },
      ],
    );
  };

  const createSignature = () => {
    let epochTimeSeconds = Math.round(new Date().getTime() / 1000).toString();

    ReactNativeBiometrics.createSignature({
      promptMessage: 'Sign in',
      payload: `${epochTimeSeconds}$some message`,
      cancelButtonText: 'отмена',
    })
      .then((resultObject) => {
        console.log('>>> resultObject', resultObject);
        const {success, signature} = resultObject;

        if (success) {
          setSignatureCreatedMsg(true);
          setfingerPrintSignatue(JSON.stringify(signature));
          console.log(signature);
          //verifySignatureWithServer(signature, payload);
        }
      })
      .catch((error) => {
        console.log('>>> error', error);
      });
  };

  const createBiometricKeys = () => {
    ReactNativeBiometrics.createKeys('Confirm fingerprint').then(
      (resultObject) => {
        const {publicKey} = resultObject;
        console.log(publicKey);

        setkeyCreatedMsg(true);
        //sendPublicKeyToServer(publicKey);
      },
    );
  };

  const checkBiometricKeys = () => {
    ReactNativeBiometrics.biometricKeysExist().then((resultObject) => {
      const {keysExist} = resultObject;

      if (keysExist) {
        setkeyExistsMsg(true);
        console.log('Keys exist');
      } else {
        setkeyExistsMsg(false);
        console.log('Keys do not exist or were deleted');
      }
    });
  };

  const deleteBiometricKeys = () => {
    ReactNativeBiometrics.deleteKeys().then((resultObject) => {
      const {keysDeleted} = resultObject;

      if (keysDeleted) {
        setkeyDeletedMsg(true);
        console.log('Successful deletion');
      } else {
        setkeyDeletedMsg(false);
        console.log(
          'Unsuccessful deletion because there were no keys to delete',
        );
      }
    });
  };

  return (
    <View style={styles.container}>
      {activeSensor && (
        <View>
          <Text style={styles.headerText}>Biometric Saved?</Text>
          <Text style={styles.text}>{activeSensor ? 'True' : 'False'}</Text>
          <Text style={styles.headerText}>Biometric type:</Text>
          <Text style={styles.text}>{activeSensor.biometryType}</Text>
        </View>
      )}

      {keyCreatedMsg && <Text>Key created</Text>}
      <Button title="Create key" onPress={createBiometricKeys}></Button>

      {keyExistsMsg && <Text>Key exists</Text>}
      <Button title="Check key" onPress={checkBiometricKeys}></Button>

      {keyDeletedMsg && <Text>Key deleted</Text>}
      <Button title="Delete key" onPress={deleteBiometricKeys}></Button>

      {signatureCreatedMsg && <Text>Signature created</Text>}
      <Text>{fingerPrintSignatue}</Text>
      <Button title="Create signature" onPress={createSignature}></Button>

      <Button
        title="Scan"
        onPress={
          Platform.OS === 'android' ? showAndroidAlert : scanFingerprint
        }></Button>
      <Text>{fingerPrintResult}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 18,
    textAlign: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 150,
    height: 60,
    backgroundColor: '#056ecf',
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 30,
    color: '#fff',
  },
});

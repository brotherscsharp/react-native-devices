import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useNetInfo} from '@react-native-community/netinfo';

export default function NetworkComponent() {
  const netInfo = useNetInfo();

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>Network state:</Text>
      <Text style={styles.text}>
        Type: {netInfo.type} Connected: {`${netInfo.isConnected}`}{' '}
        InternetReachable: {`${netInfo.isInternetReachable}  `}
        WifiEnabled: {`${netInfo.isWifiEnabled}`}
      </Text>
      {netInfo && netInfo.details && (
        <View>
          <Text style={styles.headerText}>IP address:</Text>
          <Text style={styles.text}>{netInfo.details.ipAddress}</Text>
          <Text style={styles.headerText}>SSID Wi-Fi:</Text>
          <Text style={styles.text}>{netInfo.details.ssid}</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

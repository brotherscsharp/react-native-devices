import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, DeviceEventEmitter} from 'react-native';
import {useBatteryLevel, useBatteryLevelIsLow} from 'react-native-device-info';
import {usePowerState} from 'react-native-device-info';

export const BatteryComponent = () => {
  const [bmBatteryLevel, setBatteryLevel] = useState(0);
  const [bmIsPlugged, setIsPlugged] = useState(false);

  const onBatteryStatus = (info) => {
    setBatteryLevel(info.level);
    setIsPlugged(info.isPlugged);
  };

  useEffect(() => {
    DeviceEventEmitter.addListener('BatteryStatus', onBatteryStatus);
  }, []);

  const batteryLevel = useBatteryLevel();

  const batteryIsLowPowerEnabled = useBatteryLevelIsLow();

  const powerState = usePowerState();

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>[react-native-device-info]</Text>
      <Text style={styles.headerText}>Battery Level:</Text>
      <Text style={styles.text}>{batteryLevel}</Text>

      <Text style={styles.headerText}>Battery is Low power enabled:</Text>
      <Text style={styles.text}>{batteryIsLowPowerEnabled}</Text>

      <Text style={styles.headerText}>Full state:</Text>
      <Text style={styles.text}>
        State: {powerState.batteryState} Level: {powerState.batteryLevel}{' '}
        LowPowerMode: {`${powerState.lowPowerMode}`}
      </Text>

      <Text style={styles.headerText}>[react-native DeviceEventEmitter]</Text>
      <Text style={styles.text}>
        bmBatteryLevel: {bmBatteryLevel} bmIsPlugged: {`${bmIsPlugged}`}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {
  accelerometer,
  gyroscope,
  magnetometer,
  barometer,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';

export default function SensorsComponent() {
  const [rnAccelerometer, setAccelerometerData] = useState({});
  const [rnGyroscope, setGyroscopeData] = useState({});
  const [rnMagnetometer, setMagnetometerData] = useState({});
  const [rnBarometer, setBarometerData] = useState({});

  useEffect(() => {
    if (accelerometer) {
      setUpdateIntervalForType(SensorTypes.accelerometer, 400); // defaults to 100ms

      const subscriptionAccelerometr = accelerometer.subscribe(
        ({x, y, z, timestamp}) => setAccelerometerData({x, y, z, timestamp}),
      );
    }

    if (gyroscope) {
      setUpdateIntervalForType(SensorTypes.gyroscope, 400); // defaults to 100ms

      const subscriptionGyroscope = gyroscope.subscribe(
        ({x, y, z, timestamp}) => setGyroscopeData({x, y, z, timestamp}),
      );
    }

    if (magnetometer) {
      setUpdateIntervalForType(SensorTypes.magnetometer, 400); // defaults to 100ms

      const subscriptionMagenetometer = magnetometer.subscribe(
        ({x, y, z, timestamp}) => setMagnetometerData({x, y, z, timestamp}),
      );
    }

    if (barometer) {
      setUpdateIntervalForType(SensorTypes.barometer, 400); // defaults to 100ms

      const subscription = accelerometer.subscribe(
        ({pressure}) => setBarometerData({pressure}),
        (error) => {
          console.log('The sensor is not available');
        },
      );
    }
  }, []);

  return (
    <View style={styles.sensor}>
      <Text style={styles.headerText}>
        Accelerometer: (in Gs where 1 G = 9.81 m s^-2)
      </Text>
      {rnAccelerometer ? (
        <Text style={styles.text}>
          x: {round(rnAccelerometer.x)} y: {round(rnAccelerometer.y)} z:{' '}
          {round(rnAccelerometer.z)}
        </Text>
      ) : (
        <Text style={styles.text}>x: y: z: }</Text>
      )}

      <Text style={styles.headerText}>Gyroscope:</Text>
      {rnGyroscope ? (
        <Text style={styles.text}>
          x: {round(rnGyroscope.x)} y: {round(rnGyroscope.y)} z:{' '}
          {round(rnGyroscope.z)}
        </Text>
      ) : (
        <Text style={styles.text}>x: y: z:</Text>
      )}

      <Text style={styles.headerText}>Barometer:</Text>
      {rnBarometer && rnBarometer.pressure ? (
        <Text style={styles.text}>{rnBarometer.pressure * 100} Pa</Text>
      ) : (
        <Text style={styles.text}>0 Pa</Text>
      )}

      <Text style={styles.headerText}>Magnetometer:</Text>
      {rnMagnetometer ? (
        <Text style={styles.text}>
          x: {round(rnMagnetometer.x)} y: {round(rnMagnetometer.y)} z:{' '}
          {round(rnMagnetometer.z)}
        </Text>
      ) : (
        <Text style={styles.text}>x: y: z:</Text>
      )}
    </View>
  );
}

function round(n) {
  if (!n) {
    return 0;
  }
  return Math.floor(n * 100) / 100;
}

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: 15,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    padding: 10,
  },
  middleButton: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#ccc',
  },
  sensor: {
    marginTop: 45,
    paddingHorizontal: 10,
  },
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

import React, {useState} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import Sound from 'react-native-sound';
import {AudioRecorder, AudioUtils} from 'react-native-audio';

export const AudioComponent = () => {
  const [recordingDuration, setRecordingDuration] = useState(0);
  const [lasftFilePath, setlasftFilePath] = useState('');

  let sound = new Sound('hello.mp3', Sound.MAIN_BUNDLE);
  const playAudio = () => {
    sound.play();
  };

  const pauseAudio = () => {
    sound.pause();
  };

  const stopAudio = () => {
    sound.stop();
  };

  const onRecordPressed = () => {
    let audioPath = `${
      AudioUtils.DocumentDirectoryPath
    }/record${Date.now()}.aac`;
    setlasftFilePath(audioPath);

    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: 'Low',
      AudioEncoding: 'aac',
    });
    AudioRecorder.onProgress = (data) => {
      console.log('onProgress => ', data);
      setRecordingDuration(Math.floor(data.currentTime));
    };

    AudioRecorder.startRecording();
  };

  const onPlayPausePressed = () => {
    const sound = new Sound(lasftFilePath, Sound.DOCUMENT, (error) => {
      if (error) {
        console.log('error => ', error);
      }
      sound.play();
    });
  };

  const stopRocording = () => {
    AudioRecorder.stopRecording();
  };

  return (
    <View>
      <View>
        <Text style={styles.headerText}>Audio player</Text>
        <Button title="Play" onPress={() => playAudio()} />
        <Button title="Pause" onPress={() => pauseAudio()} />
        <Button title="Stop" onPress={() => stopAudio()} />

        <Text style={styles.headerText}>Microphone</Text>
        <Text style={styles.text}>recording: {recordingDuration} sec.</Text>
        <Button title="Record" onPress={() => onRecordPressed()} />
        <Button title="Play" onPress={() => onPlayPausePressed()} />
        <Button title="Stop" onPress={() => stopRocording()} />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
  },
  headerText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

import React, {useState, useEffect} from 'react';
import {Platform, Text, View, StyleSheet} from 'react-native';
import Geolocation from '@react-native-community/geolocation';

export default function GeolocationComponent() {
  const [location, setLocation] = useState(null);
  const [currentLocation, setCurrentLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState('');

  useEffect(() => {
    (async () => {
      //Geolocation.getCurrentPosition(info => setLocation(info));

      Geolocation.getCurrentPosition(
        (position) => {
          const info = JSON.stringify(position);
          setLocation(info);
        },
        (error) => Alert.alert('Error', JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
      );

      this.watchID = Geolocation.watchPosition((position) => {
        const lastPosition = JSON.stringify(position);
        setCurrentLocation(lastPosition);
      });
    })();
  });

  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }
  let currentPosition = '';
  if (errorMsg) {
    currentPosition = errorMsg;
  } else if (currentLocation) {
    currentPosition = JSON.stringify(currentLocation);
  }

  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>{text}</Text>
      <Text style={styles.paragraph}>{currentPosition}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
});

import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import {HomeScreen} from './src/screens/HomeScreen';
import {CameraScreen} from './src/screens/CameraScreen';
import {SensorsScreen} from './src/screens/SensorsScreen';
import {GeolocationScreen} from './src/screens/GeolocationScreen';
import {BatteryScreen} from './src/screens/BatteryScreen';
import {AudioScreen} from './src/screens/AudioScreen';
import {NetworkScreen} from './src/screens/NetworkScreen';
import {BluetoothScreen} from './src/screens/BluetoothScreen';
import {NfcScreen} from './src/screens/NfcScreen';
import {FingerprintScreen} from './src/screens/FingerprintScreen';

const Drawer = createDrawerNavigator();
export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Drawer.Navigator initialRouteName="Home">
          <Drawer.Screen name="Home" component={HomeScreen} />
          <Drawer.Screen name="Camera" component={CameraScreen} />
          <Drawer.Screen name="Geolocation" component={GeolocationScreen} />
          <Drawer.Screen name="Sensors" component={SensorsScreen} />
          <Drawer.Screen name="Battery" component={BatteryScreen} />
          <Drawer.Screen name="Audio" component={AudioScreen} />
          <Drawer.Screen name="Network" component={NetworkScreen} />
          <Drawer.Screen name="Bluetooth" component={BluetoothScreen} />
          <Drawer.Screen name="NFC" component={NfcScreen} />
          <Drawer.Screen name="Fingerprint" component={FingerprintScreen} />
        </Drawer.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
